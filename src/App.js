import './assets/css/App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LayoutComponent from './modules/layout';
import AboutUsPage from './modules/about-us';
import HomePage from './modules/home';
import OurServicePage from './modules/our-service';
import FeatureComponent from './modules/features';
import OurTeamPage from './modules/our-team';
import TestimonailPage from './modules/testimonial';
import ContactUsPage from './modules/contact-us';
import NotFoundPage from './modules/not-found';
import FreeQuotePage from './modules/free-quote';


function App() {
  return (
    <>
     <BrowserRouter>
      <Routes>
        <Route path='/' element={<LayoutComponent />}>
          <Route index element={<HomePage />} />
          <Route path='about-us' element={<AboutUsPage />} />
          <Route path='our-service' element={<OurServicePage />} />
          <Route path='feature' element={<FeatureComponent />} />
          <Route path='our-team' element={<OurTeamPage />} />
          <Route path='testimonial' element={<TestimonailPage />} />
          <Route path='contact-us' element={<ContactUsPage />} />
          <Route path='free-quote' element={<FreeQuotePage />} />
          <Route path="*" element={<NotFoundPage url="/" />} />
        </Route>
      </Routes>
     
    </BrowserRouter>
    
    </>
   
  );
}

export default App;
