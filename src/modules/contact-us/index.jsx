import React from "react";
import SpinnerPage from "../component/Spinner";
import BannerComponent from "../component/banner";
import { Link } from "react-router-dom";
import { Col, Container, Form, Row } from "react-bootstrap";

const ContactUsPage = () => {
  return (
    <>
      <SpinnerPage />
      <BannerComponent manuname="Contact Us"/>
      <div className="container-xxl py-5">
        <div className="container">
          <div className="row g-5">
            <div className="col-lg-6 wow fadeIn">
              <h1 className="display-6 mb-5 fw-bold">
                If You Have Any Querys, Please Contact Us
              </h1>
              <p className="mb-4 text-secondary">
                The contact form is currently inactive. Get a functional and
                working contact form with Ajax & PHP in a few minutes. Just copy
                and paste the files, add a little code and you're done.
                <span>
                  <Link
                    to="https://htmlcodex.com/contact-form"
                    className="text-decoration-none ms-2 text-contact"
                  >
                    Download Now
                  </Link>
                </span>
              </p>
              <Container>
                <Row>
                  <Col sm={6}>
                    <Form.Control
                      type="text"
                      placeholder="Your Name"
                      className="px-2 py-3 rounded-0 border border-1 fs-6 mb-3"
                    />
                  </Col>
                  <Col>
                    <Form.Control
                      type="email"
                      placeholder="Your Email"
                      className="px-2 py-3 rounded-0 border border-1 fs-6"
                    />
                  </Col>
                </Row>
                <Row className="mt-3">
                  <Col>
                    <Form.Control
                      type="text"
                      placeholder="Subject"
                      className="px-2 py-3 rounded-0 border border-1 fs-6"
                    />
                  </Col>
                </Row>
                <Row className="mt-4">
                  <Col>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Message"
                      className="px-2 py-3 rounded-0 border border-1 fs-6"
                    />
                  </Col>
                </Row>
                <Link
                  to=""
                  className="align-self-start btn btn-primary py-3 px-5 rounded-0 mt-3"
                >
                  Send Message
                </Link>
              </Container>
            </div>
            <div className="col-lg-6 wow fadeIn">
              <div className="position-relative overflow-hidden h-100">
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3001156.4288297426!2d-78.01371936852176!3d42.72876761954724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4ccc4bf0f123a5a9%3A0xddcfc6c1de189567!2sNew%20York%2C%20USA!5e0!3m2!1sen!2sbd!4v1603794290143!5m2!1sen!2sbd"
                  style={{}}
                  title="Embedded Content"
                  className="position-relative w-100 h-100"
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContactUsPage;
