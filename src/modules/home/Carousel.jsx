// Import necessary dependencies and assets
import React, { useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";
import pic from "../../assets/img/carousel-1.jpg";
import pic1 from "../../assets/img/carousel-2.jpg";
import { Link } from "react-router-dom";
const Carousels = () => {
    useEffect(() => {
      AOS.init();
    }, []);

  return (
    <div className="container-fluid p-0 mb-5">
      <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img src={pic} className="d-block w-100" alt="..." />
            <div className="carousel-caption">
              <div className="container">
                <div className="row justify-content-center">
                  <div className="col-lg-7 pt-5">
                    <h1 className="display-4 text-white mb-4" data-aos="fade-down">We Provide Best AC Repair Services</h1>
                    <p className="fs-5 mb-4 pb-2 mx-sm-5 text" data-aos="fade-down">
                      Aliqu diam amet diam et eos. Clita erat ipsum et lorem sed stet lorem sit clita duo justo erat amet
                    </p>
                    <Link to="#" className="btn btn-primary py-3 px-5 text-decoration-none" data-aos="fade-down">
                      Explore More
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="carousel-item">
            <img src={pic1} className="d-block w-100" alt="..." />
            <div className="carousel-caption">
              <div className="container">
                <div className="row justify-content-center">
                  <div className="col-lg-7 pt-5">
                    <h1 className="display-4 text-white mb-4" data-aos="fade-down">Quality Heating & Air Condition Services</h1>
                    <p className="fs-5 mb-4 pb-2 mx-sm-5 text" data-aos="fade-down">
                      Aliqu diam amet diam et eos. Clita erat ipsum et lorem sed stet lorem sit clita duo justo erat amet
                    </p>
                    <Link to="#" className="btn btn-primary py-3 px-5 text-decoration-none" data-aos="fade-down">
                      Explore More
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <button className="carousel-control-prev" type="button" style={{ zIndex: "unset" }} data-bs-target="#carouselExampleControls" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true" style={{ width: "3rem", height: "3rem" }}></span>
          <span className="visually-hidden"></span>
        </button>
        <button className="carousel-control-next" type="button" style={{ zIndex: "unset" }} data-bs-target="#carouselExampleControls" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true" style={{ width: "3rem", height: "3rem" }}></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    </div>
   

  );
};

export default Carousels;
