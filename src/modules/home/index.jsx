import React from "react";
// import slide1 from "../../assets/img/carousel-1.jpg";
// import slide2 from "../../assets/img/carousel-2.jpg";
import SpinnerPage from "../component/Spinner";
import AboutPage from "../about-us/AboutPage";
import Feature from "../features/feature";
import Professional from "../our-service/professional";
import FreeQuoteComponent from "../free-quote/free-quote";
import AboutpageComponent from "../component/aboutpageCp";
import TestimonailComponent from "../testimonial/testimonial";
import Carousels from "./Carousel";
// import { Carousel } from "react-bootstrap";

const HomePage = () => {
  return (
    <>
     <Carousels/>
      <SpinnerPage />
      <AboutPage />
      <Feature />
      <Professional />
      <FreeQuoteComponent />
      <AboutpageComponent />
      <TestimonailComponent />
      
    </>
  );
};

export default HomePage;
