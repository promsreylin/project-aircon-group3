import React from "react";
import BannerComponent from "../component/banner";
import SpinnerPage from "../component/Spinner";
import Feature from "./feature";
const FeatureComponent = () => {
 
  return (
    <>
      <SpinnerPage />
      <BannerComponent manuname="Feature"/>
      <Feature/>
    </>
  );
};

export default FeatureComponent;
