import React from 'react'
import pic from "../../assets/img/feature.jpg";
import pic1 from "../../assets/img/icon-08-light.jpg";
import pic2 from "../../assets/img/icon-10-light.png";
import pic3 from "../../assets/img/icon-06-light.png"
import { Fade } from "../component/aos";
const Feature = () => {
    const Data = [
        {
          id: 1,
          src: pic1,
          text1: "Trusted Service Center",
          text2:  "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit. Aliqu diam amet diam et eos.",
        },
        {
          id: 2,
    
          src: pic2,
          text1: "Reasonable Price",
          text2:   "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit. Aliqu diam amet diam et eos.",
        },
        {
          id: 3,
          src: pic3,
          text1: "24/7 Supports",
          text2: "Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit. Aliqu diam amet diam et eos.",
        },
      ];
  return (
    <div className="container-xxl py-5">
        <div className="container">
          <div className="row d-flex">
            <div className="col-lg-6">
              <Fade styleaos="fade-up" delay="500">
                <p className="text2 fw-bold mb-5 text-black">
                  Few Reasons Why People Choosing Us!
                </p>
                <p className="mb-5 text1 fs-6">
                  Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit.
                  Aliqu diam amet diam et eos. Clita erat ipsum et lorem et sit,
                  sed stet lorem sit clita duo justo magna dolore erat amet
                </p>
                <div className="mb-5 d-block">
                  {Data.map((dt) => (
                    <div className="d-flex mb-5" key={dt.id}>
                      <div className="btn-square align-items-center justify-content-center p-3 rounded-circle">
                        <img src={dt.src} alt="" />
                      </div>
                      <div className="ms-4">
                        <p className="mb-3 fs-5 fw-bold text-black">{dt.text1}</p>
                        <p className="text1">{dt.text2}</p>
                      </div>
                    </div>
                  ))}
                </div>
              </Fade>
            </div>
            <div className="col-lg-6">
              <Fade styleaos="fade-up" delay="800">
                <div className="position-lg-relative roundeds h-100">
                  <img
                    src={pic}
                    alt=""
                    className="position-lg-absolute w-100 h-100 object-fit-cover"
                  />
                </div>
              </Fade>
            </div>
          </div>
        </div>
      </div>
  )
}

export default Feature
