import React from 'react'
import TeamCard from "../our-team/TeamCard";
import TeamData from "../data/TeamData";
const AboutpageComponent = () => {
    const firstFourTeamMembers = TeamData.slice(0, 4);
  return (
    <div className="container mt-5">
    <div className="mb-5 d-flex justify-content-center align-items-center">
      <h1 className=" text-center fw-bold mt-5" style={{ width: "500px" }}>
        Meet Our Professional Team Members
      </h1>
    </div>
    <div className="row">
      {firstFourTeamMembers.map((val, ind) => {
        return (
          <TeamCard {...val} key={ind} delayDura={(ind + 1) * 200 + 500} />
        );
      })}
    </div>
  </div>
  )
}

export default AboutpageComponent