import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

// FadeUp
export const Fade = (props) => {
  const { children, styleaos, delay, placement } = props;
  useEffect(() => {
    AOS.init({
        duration: 1200,
        once: true,
        easing: 'ease-in-out-back'});
    AOS.refresh();
  }, []);
  return (
    <div data-aos={styleaos}  data-aos-delay={delay} data-aos-anchor-placement={placement}>
      {children}
    </div>
  );
};