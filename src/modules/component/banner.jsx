import React from "react";
import { Fade } from "./aos";


const BannerComponent = ({manuname}) => {

 
  return (
    <div>
      <section className="image-text-container position-relative overflow-hidden w-100 mb-5 bg-black py-5" style={{height:"310px"}}>
        <div className="contain-fluid text-center p-5 text-overlay position-absolute text-white">
          <div className="">
          <div>
            <Fade
              styleaos="fade-down"
              delay="300"
            >

              <h1 className="texts mb-4">{manuname}</h1>

            </Fade>
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb justify-content-center m-0">
                <li className="breadcrumb-item">Home</li>
                <li className="breadcrumb-item">Pages</li>
                <li className="breadcrumb-item features">{manuname}</li>
              </ol>
            </nav>
          </div>
        </div>
        </div>
        
      </section>
    </div>
  );
};

export default BannerComponent;
