import React, { useState, useEffect }  from 'react'
import { Spinner } from 'react-bootstrap';

const SpinnerPage = () => {
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        // Set loading to true when the refresh starts
        setLoading(true);
      
        // Your refresh logic goes here (e.g., fetching data, API calls)
      
        // Simulate a delay (you can replace this with your actual refresh logic)
        const delay = setTimeout(() => {
          // Set loading to false when the refresh is complete
          setLoading(false);
        }, 1000); // Simulated delay of 2 seconds
      
        // Clean up the timeout to avoid memory leaks
        return () => clearTimeout(delay);
      }, []); // Empty dependency array means this effect runs once when the component mounts
  return (
    <div>
    {loading ? (
     <div className="bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center" id="spinner">
       <Spinner animation="grow"/>
    </div>
    ) : (
      // Your actual content goes here
      <div>
        <h1 className="visually-hidden">Your Content Hererer</h1>
      </div>
    )}
  </div>
  )
}

export default SpinnerPage