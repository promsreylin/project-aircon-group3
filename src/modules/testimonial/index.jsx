import React from "react";
import SpinnerPage from "../component/Spinner";
import BannerComponent from "../component/banner";
import TestimonailComponent from "./testimonial";



const TestimonailPage = () => {
 
  return (
    <>
      <SpinnerPage />
      <BannerComponent manuname="Testimonial" />
      <TestimonailComponent/>
    </>
  );
};

export default TestimonailPage;
