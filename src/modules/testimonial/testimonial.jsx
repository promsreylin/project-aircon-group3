import React from "react";
import pic1 from "../../assets/img/testimonial-1.jpg";
import pic2 from "../../assets/img/testimonial-2.jpg";
import pic3 from "../../assets/img/testimonial-3.jpg";
import OwlCarousel from "react-owl-carousel";
import { Fade } from "../component/aos";

const TestimonailComponent = () => {
  const options = {
    items: 1, // Show only one slide
    loop: true, // Enable loop
    nav: true, // Show navigation buttons
  };
  return (
    <div className="container-xxl py-5">
      <div className="container">
        <Fade styleaos="fade-up" delay="500">
          <div className="text-center mx-auto">
            <h1 className="mb-5 w-50 mx-auto">
              What They Say About Our Services
            </h1>
          </div>
        </Fade>
        <div className="row g-5">
          <div className="col-lg-3 d-none d-lg-block position-relative">
            <div className="img-fluid position-absolute animateds">
              <img
                id=""
                src={pic1}
                alt=""
                className="pulse-image rounded-circle animateds"
              />
            </div>
            <div className="img-fluid position-absolute animateded">
              <img
                id=""
                src={pic2}
                alt=""
                className="pulse-image rounded rounded-circle animateded"
              />
            </div>
            <div className="img-fluid position-absolute animated">
              <img
                id=""
                src={pic3}
                alt=""
                className="pulse-image rounded rounded-circle animated"
              />
            </div>
          </div>
          <div className="col-lg-6 text-center">
            <OwlCarousel
              className="owl-theme owl1"
              {...options}
              autoplay="true"
            >
              <div className="item">
                <div className="testimonial-item text-center">
                  <img
                    src={pic1}
                    alt=""
                    className="img-fluid mx-auto mb-4 rounded-circle img"
                  />
                  <p className="fs-5 text-secondary">
                    Dolores sed duo clita tempor justo dolor et stet lorem kasd
                    labore dolore lorem ipsum. At lorem lorem magna ut et,
                    nonumy et labore et tempor diam tempor erat.
                  </p>
                  <h5>Client Name</h5>
                  <p className="fs-6 text-secondary">Profession</p>
                </div>
              </div>
              <div className="item">
                <div className="testimonial-item text-center">
                  <img
                    src={pic2}
                    alt=""
                    className="img-fluid mx-auto mb-4 rounded-circle img"
                  />
                  <p className="fs-5 text-secondary">
                    Dolores sed duo clita tempor justo dolor et stet lorem kasd
                    labore dolore lorem ipsum. At lorem lorem magna ut et,
                    nonumy et labore et tempor diam tempor erat.
                  </p>
                  <h5>Client Name</h5>
                  <p className="fs-6 text-secondary">Profession</p>
                </div>
              </div>
              <div className="item">
                <div className="testimonial-item text-center">
                  <img
                    src={pic3}
                    alt=""
                    className="img-fluid mx-auto mb-4 rounded-circle img"
                  />
                  <p className="fs-5 text-secondary">
                    Dolores sed duo clita tempor justo dolor et stet lorem kasd
                    labore dolore lorem ipsum. At lorem lorem magna ut et,
                    nonumy et labore et tempor diam tempor erat.
                  </p>
                  <h5>Client Name</h5>
                  <p className="fs-6 text-secondary">Profession</p>
                </div>
              </div>
            </OwlCarousel>
          </div>
          <div className="col-lg-3 d-none d-lg-block position-relative">
            <div className="position-absolute animateds">
              <img
                src={pic3}
                alt=""
                className="pulse-images animateds rounded-circle"
              />
            </div>

            <div className="position-absolute animateded">
              <img
                src={pic2}
                alt=""
                className="pulse-image rounded-circle animateded"
              />
            </div>
            <div className="position-absolute animated">
              <img
                src={pic1}
                alt=""
                className="pulse-image rounded-circle animated"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TestimonailComponent;
