import service1 from "../../assets/img/service-1.jpg";
import service2 from "../../assets/img/service-2.jpg";
import service3 from "../../assets/img/service-3.jpg";
import service4 from "../../assets/img/service-4.jpg";
import service5 from "../../assets/img/service-5.jpg";
import service6 from "../../assets/img/service-6.jpg";

import icon1 from "../../assets/img/icon-01-light.png";
import icon2 from "../../assets/img/icon-02-light.png";
import icon3 from "../../assets/img/icon-03-light.png";
import icon4 from "../../assets/img/icon-04-light.png";
import icon5 from "../../assets/img/icon-05-light.png";
import icon6 from "../../assets/img/icon-06-light.png";

const ServiceData = [
  { serviceImgSrc: service1, icon: icon1, position: "AC Installation" },
  { serviceImgSrc: service2, icon: icon2, position: "Cooling Services" },
  { serviceImgSrc: service3, icon: icon3, position: "Heating Services" },

  { serviceImgSrc: service4, icon: icon4, position: "Maintenance & Repair" },
  { serviceImgSrc: service5, icon: icon5, position: "Indoor Air Quality" },
  { serviceImgSrc: service6, icon: icon6, position: "Annual Inspections" },
];

export default ServiceData;
