import Team1 from "../../assets/img/team-1.jpg";
import Team2 from "../../assets/img/team-2.jpg";
import Team3 from "../../assets/img/team-3.jpg";
import Team4 from "../../assets/img/team-4.jpg";


const TeamData = [
    { teamImgSrc: Team1, name: "Employee 1", position: "Designer" },
    { teamImgSrc: Team2, name: "Employee 2", position: "Designer" },
    { teamImgSrc: Team3, name: "Employee 3", position: "Designer" },
    { teamImgSrc: Team4, name: "Employee 4", position: "Designer" },

    { teamImgSrc: Team4, name: "Employee 5", position: "Designer" },
    { teamImgSrc: Team2, name: "Employee 6", position: "Designer" },
    { teamImgSrc: Team3, name: "Employee 7", position: "Designer" },
    { teamImgSrc: Team1, name: "Employee 8", position: "Designer" },
];

export default TeamData;