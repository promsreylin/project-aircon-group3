import React from "react";
import BannerComponent from "../component/banner";
import SpinnerPage from "../component/Spinner";
import FreeQuoteComponent from "./free-quote";

const FreeQuotePage = () => {
  return (
    <div>
      <SpinnerPage />
      <BannerComponent manuname="Free Quote"/>
      <FreeQuoteComponent/>
      
    </div>
  );
};

export default FreeQuotePage;
