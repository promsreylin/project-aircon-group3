import React from 'react'
import pic1 from "../../assets/img/carousel-1.jpg";
import pic2 from "../../assets/img/carousel-2.jpg";
import { Col, Container, Form, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { Parallax } from "react-parallax";

const FreeQuoteComponent = () => {
  return (
    <div className="container-fluid overflow-hidden px-lg-0">
        <div className="container quote px-lg-0">
          <div className="row g-0 position-relative sm:overflow-x-hidden">
            <div className="col-lg-6 mt-5">
              <div className="position-relative">
                <Parallax
                  blur={0}
                  bgImage={pic1}
                  bgImageAlt=""
                  strength={300}
                  style={{ height: "580px" }}
                ></Parallax>
                <div className="overlay">
                  <div
                    className="h-100 qout-text position-absolute text-right"
                    style={{ bottom: "-25%", left: "8%" }}
                  >
                    <p className="fs-1 fw-bold text-white">
                      For Individuals And <br /> Organisations
                    </p>
                    <p className="text-white">
                      Tempor erat elitr rebum at clita. Diam dolor diam ipsum
                      sit. Aliqu diam amet diam et eos. <br /> Clita erat ipsum
                      et lorem et <br /> sit, sed stet lorem sit clita duo justo
                      magna dolore erat amet
                    </p>
                    <Link
                      to=""
                      className="align-self-start btn btn-primary py-3 px-5 rounded-0 mt-5 btn"
                    >
                      More Details
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6 mt-5">
              <div className="position-relative">
                <Parallax
                  bgImage={pic2}
                  bgImageAlt=""
                  strength={300}
                  style={{ height: "580px" }}
                ></Parallax>
                <div className="overlays">
                  <div
                    className="position-absolute bg-white custom-padding"
                    style={{ bottom: "10%", left: "8%", right: "8%" }}
                  >
                    <Container>
                      <Row>
                        <Col sm={6} className="mb-3">
                          <Form.Control
                            type="text"
                            placeholder="Your Name"
                            className="px-2 py-3 rounded-0 border border-1 fs-6"
                          />
                        </Col>
                        <Col>
                          <Form.Control
                            type="email"
                            placeholder="Your Email"
                            className="px-2 py-3 rounded-0 border border-1 fs-6"
                          />
                        </Col>
                      </Row>
                      <Row className="mt-3">
                        <Col sm={6} className="mb-3">
                          <Form.Control
                            type="text"
                            placeholder="Your Mobile"
                            className="px-2 py-3 rounded-0 border border-1 fs-6"
                          />
                        </Col>
                        <Col className="sm:mt-3">
                          <Form.Control
                            type="text"
                            placeholder="Service Type"
                            className="px-2 py-3 rounded-0 border border-1 fs-6"
                          />
                        </Col>
                      </Row>
                      <Row className="mt-3">
                        <Col>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            placeholder="Message"
                            className="px-2 py-3 rounded-0 border border-1 fs-6"
                          />
                        </Col>
                      </Row>
                      <Link
                        to=""
                        className="align-self-start btn btn-primary py-3 px-5 rounded-0 mt-3 btn"
                      >
                        Get A Free Quote
                      </Link>
                    </Container>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  )
}

export default FreeQuoteComponent