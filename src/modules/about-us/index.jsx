import React from "react";
import SpinnerPage from "../component/Spinner";
import BannerComponent from "../component/banner";
import AboutPage from "./AboutPage";
import AboutpageComponent from "../component/aboutpageCp";

const AboutUsPage = () => {
  return (
    <>
      <SpinnerPage />
      <BannerComponent manuname="About"/>
      <AboutPage />
      <AboutpageComponent/>
    </>
  );
};

export default AboutUsPage;
