import React from "react";
import icon1 from "../../assets/img/icon-07-primary.png";
import icon2 from "../../assets/img/icon-09-primary.png";
import image1 from "../../assets/img/about-1.jpg";
import image2 from "../../assets/img/about-2.jpg";
import image3 from "../../assets/img/about-3.jpg";
import image4 from "../../assets/img/about-4.jpg";
import { Fade } from "../component/aos";
import CountUp from "react-countup";
export function AboutPage() {
 

  return (
    <>
      <div className="container">
        <div className="row">
          <div className=" col-6 mt-4 pt-4">
            <Fade styleaos="fade-up" delay="500">
              <h1 className="title mt-4 pt-4">
                Welcome To Best Cooling & Heating Service Center
              </h1>
              <div>
                <div className=" justify-content-between  mt-5">
                  <div className="d-flex align-items-center">
                    <img src={icon1} alt="Expert Technician" />
                    <h5 className=" mx-4">Expert Technician</h5>
                    <img src={icon2} alt="Expert Technician" />
                    <h5 className=" mx-4">Best Quality Services</h5>
                  </div>
                  <p className=" text-secondary mb-4 mt-4">
                    Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit.
                    Aliqu diam amet diam et eos. Clita erat ipsum et lorem et
                    sit, sed stet lorem sit clita duo justo magna dolore erat
                    amet
                  </p>
                  <div className="row border-top mt-4 pt-4">
                    <div className="col-md-6 col-12 d-flex">
                      <i className="fa-solid fa-phone  rounded-circle p-3 text-white"></i>
                      <div>
                        <h4 className=" mx-4 text-center mt-2 ">
                          +012 345 6789
                        </h4>
                      </div>
                    </div>
                    <div className="col-md-6 col-12 d-flex">
                      <i className="fa-regular fa-envelope rounded-circle p-3 text-white"></i>
                      <h4 className=" mx-4 text-center mt-2 ">
                        info@example.com
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </Fade>
          </div>
          <div className=" col-6 d-flex g-4">
            <div className="row">
              <div className="col-6  text-end  mt-4 pt-4 ">
                <Fade styleaos="zoom-in" delay="100">
                  <img className="w-75  mt-2 pt-3 " src={image1} alt="" />
                </Fade>
              </div>
              <div className="col-6 text-strat">
                <Fade styleaos="zoom-in" delay="300">
                  <img className="w-100 mb-3" src={image2} alt="" />
                </Fade>
              </div>
              <div className="col-6 text-end ">
                <Fade styleaos="zoom-in" delay="500">
                  <img className="w-50 " src={image3} alt="" />
                </Fade>
              </div>
              <div className="col-6 text-start">
                <Fade styleaos="zoom-in" delay="700">
                  <img className="w-75" src={image4} alt="" />
                </Fade>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section>
        <div className=" container-fluid my-5 py-5">
          <div className="container">
            <div className="row py-5">
              <div className="col-sm-6 col-lg-3">
                <Fade styleaos="fade-in" delay="100">
                  <h1>
                    <CountUp
                      className=" text-light"
                      start={0}
                      end={1234}
                      duration={2}
                    />
                  </h1>
                  <span className="sub-title">Happy Clients</span>
                </Fade>
              </div>
              <div className="col-sm-6 col-lg-3">
                <Fade styleaos="fade-in" delay="300">
                  <h1>
                    <CountUp
                      className=" text-light"
                      start={0}
                      end={1234}
                      duration={3}
                    />
                  </h1>
                  <span className="sub-title">Projects Succeed</span>
                </Fade>
              </div>
              <div className="col-sm-6 col-lg-3">
                <Fade styleaos="fade-in" delay="500">
                  <h1>
                    <CountUp
                      className=" text-light"
                      start={0}
                      end={1234}
                      duration={4}
                    />
                  </h1>
                  <span className="sub-title">Awards Achieved</span>
                </Fade>
              </div>
              <div className="col-sm-6 col-lg-3">
                <Fade styleaos="fade-in" delay="700">
                  <h1>
                    <CountUp
                      className=" text-light"
                      start={0}
                      end={1234}
                      duration={4}
                    />
                  </h1>
                  <span className="sub-title">Team Members</span>
                </Fade>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default AboutPage;
