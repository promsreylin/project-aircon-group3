import React from 'react'

import SpinnerPage from '../component/Spinner'
import BannerComponent from '../component/banner'
import Professional from './professional'

function OurServicePage() {
  return (
    <>
      <BannerComponent manuname="Service" />
      <Professional />
      <SpinnerPage />
    </>

  )
}

export default OurServicePage