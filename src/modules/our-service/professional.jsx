import ServiceData from "../data/ServiceData";
import ProfessionalCard from "./professionalCard";



export function Professional() {
  const firstFourTeamMembers = ServiceData.slice(0, 8);

  return (
    <div className="container">
      <div className="mb-5 d-flex justify-content-center align-items-center">
        <h1 className="text-center fw-bold" style={{ width: "500px" }}>
          We Provide professional Heating & Cooling Services
        </h1>
      </div>
      <div className="row">
        {firstFourTeamMembers.map((val, ind) => {
          return (
            <ProfessionalCard {...val} key={ind} delayDura={(ind + 1) * 200 + 500} />
          );
        })}
      </div>
    </div>
  );
}

export default Professional;