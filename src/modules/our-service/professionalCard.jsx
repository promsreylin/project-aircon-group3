
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import s from "../../assets/css/ourservice.css";
import { Fade } from "../component/aos";

export function ProfessionalCard({ serviceImgSrc, icon, position, delayDura }) {
  return (
    <div className="col-lg-4 col-md-6 mb-4">
      <Fade styleaos="fade-up" delay={`${delayDura ? delayDura : "500"}`}>
        <Card style={{ borderRadius: "0" }}>
          <Card.Img
            style={{ borderRadius: "0" }}
            variant="top"
            src={serviceImgSrc}
          />
          <div className="d-flex align-items-center bg-light">
            <div className={`flex-shrink-0 m-0 ${s.bgColor} ${s.iconservice}`}>
              <img className="img-fluid" src={icon} alt="" />
            </div>
            <Link className="text-decoration-none text-dark mx-4 fs-5 fw-bold">
              <h4>{position}</h4>
            </Link>
          </div>
        </Card>
      </Fade>
    </div>
  );
}

export default ProfessionalCard;
