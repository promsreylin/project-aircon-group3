import TeamCard from "./TeamCard";
import TeamData from "../data/TeamData";

export function Team() {
  const firstFourTeamMembers = TeamData.slice(0, 8);

  return (
    <div className="container">
      <div className="mb-5 d-flex justify-content-center align-items-center">
        <h1 className=" text-center fw-bold " style={{ width: "500px" }}>
          Meet Our Professional Team Members
        </h1>
      </div>
      <div className="row">
        {firstFourTeamMembers.map((val, ind) => {
          return (
            <TeamCard {...val} key={ind} delayDura={(ind + 1) * 200 + 500} />
          );
        })}
      </div>
    </div>
  );
}
