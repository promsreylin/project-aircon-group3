import React from "react";
import { Link } from "react-router-dom";
import { Fade } from "../component/aos";
import s from "./style.module.css"

const TeamCard = ({ teamImgSrc, name, position, delayDura }) => {
  return (
    <>
      
      <div className="col-lg-3 colo-md-6 mb-4">
        <Fade styleaos="fade-up" delay={`${delayDura ? delayDura : "500"}`}>
          <div className={` ${s.cardd}`}>
            <div className={`${s.img} overflow-hidden position-relative`}>
              <img className="img-fluid" src={teamImgSrc} alt="" />

              <div className={`text-center ${s.social}`}>
                <Link>
                  <i className="fa-brands fa-linkedin-in rounded-circle  bg-black m-2  p-2"></i>
                </Link>
                <Link>
                  <i className="fa-brands fa-twitter fa-facebook-f rounded-circle m-2 bg-black p-2"></i>
                </Link>
                <Link>
                  <i className="fa-brands fa-instagram fa-facebook-f rounded-circle m-2 bg-black p-2"></i>
                </Link>
              </div>
            </div>
            <div className={`${s.team_desc} text-center p-3 bg-light`}>
              <h4>{name}</h4>
              <span>{position}</span>
            </div>
          </div>
        </Fade>
      </div>
    </>
  );
};

export default TeamCard;
