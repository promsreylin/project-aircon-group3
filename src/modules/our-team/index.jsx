import React from "react";
import SpinnerPage from "../component/Spinner";
import BannerComponent from "../component/banner";
import { Team } from "./team";

const OurTeamPage = () => {
  return (
    <>
      <SpinnerPage />
      <BannerComponent manuname="Our Team" />
     
        <Team />
      
    </>
  );
};

export default OurTeamPage;
