import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { faEnvelopeOpen } from "@fortawesome/free-regular-svg-icons";
import pic from "../../../assets/img/logo.jpg";
import { Navbar, Nav, Container, NavDropdown } from "react-bootstrap";
import {
  faFacebookF,
  faInstagram,
  faLinkedinIn,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { Link } from "react-router-dom";
import SpinnerPage from "../../component/Spinner";

const HeaderComponent = () => {
  const Data = [
    {
      id: 1,
      icon: <FontAwesomeIcon icon={faPhone} />,
      text: "+012 345 6789",
    },
    {
      id: 2,
      icon: <FontAwesomeIcon icon={faEnvelopeOpen} />,
      text: "info@example.com",
    },
  ];
  const [showDropdown, setShowDropdown] = useState(false);

  const handleMouseEnter = () => {
    setShowDropdown(true);
  };

  const handleMouseLeave = () => {
    setShowDropdown(false);
  };
  return (
    <>
      <SpinnerPage />
      <div className="container-fluid bg-darks py-2 px-0 text-white-50 d-none d-lg-block">
        <div className="row align-items-center">
          <div className="col-lg-7 text-white-50 px-5 text-start fs-6 fw-bold">
            <div className="d-flex ">
              {Data.map((dt) => (
                <div key={dt.id}>
                  <small className="h-100 d-inline-flex me-2 ms-2">
                    {dt.icon}
                  </small>
                  <small className="h-100 d-inline-flex me-4">{dt.text}</small>
                </div>
              ))}
            </div>
          </div>
          <div className="col-lg-5 px-5 m-0 main">
            <nav aria-label="breadcrumb align-items-center">
              <ol className="breadcrumb justify-content-end m-0">
                <li className="breadcrumb-item">Home</li>
                <li className="breadcrumb-item">Terms</li>
                <li className="breadcrumb-item">Privacy</li>
                <li className="breadcrumb-item">Support</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
      <Navbar
        collapseOnSelect
        expand="lg"
        className="bg-white m-0 z-1 position-sticky top-0"
      >
        <Container className="m-0 px-4 px-lg-5 d-flex">
          <Navbar.Brand href="#home" className="Aircon fs-1 fw-bold text-start">
            <img src={pic} alt="" className="img-fluid me-3"></img>AirCon
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav" className="m-0">
            <Nav className="bg-light pe-4 py-3 py-lg-0 mx-auto">
              <Link
                id="NavLink"
                to="/"
                className="px-0 ms-4 text-secondary ggg py-3"
              >
                Home
              </Link>
              <Link
                id="NavLink"
                to="about-us"
                className="px-0 ms-4 text-secondary ggg py-3"
              >
                About Us
              </Link>
              <Link
                id="NavLink"
                to="our-service"
                className="px-0 ms-4 text-secondary ggg py-3"
              >
                Our services
              </Link>
              <NavDropdown
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
                show={showDropdown}
                title="Pages"
                id="collapsible-nav-dropdown"
                className="px-0 ms-4 py-0 text-secondary py-2"
              >
                <Link to="feature" id="link" className="py-2">
                  Features
                </Link>
                <Link to="free-quote" id="link" className="py-2">
                  Free Quote
                </Link>
                <Link to="our-team" id="link" className="py-2">
                  Our Team
                </Link>
                <Link to="testimonial" id="link" className="py-2">
                  Testimonial
                </Link>
                <Link to="*" id="link" className="py-2">
                  404 Pages
                </Link>
              </NavDropdown>
              <Link
                id="NavLink"
                to="contact-us"
                className="px-0 ms-4 text-secondary ggg active p-3"
              >
                Contact Us
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
        <div className="h-100 d-lg-inline-flex align-items-center d-none">
          <FontAwesomeIcon
            icon={faFacebookF}
            className=" bg-light rounded-circle p-2 text-primarys me-2"
          />
          <FontAwesomeIcon
            icon={faTwitter}
            className=" bg-light rounded-circle p-2 text-primarys me-2"
          />
          <FontAwesomeIcon
            icon={faLinkedinIn}
            className=" bg-light rounded-circle p-2 text-primarys me-2"
          />
          <FontAwesomeIcon
            icon={faInstagram}
            className=" bg-light rounded-circle p-2 text-primarys me-2"
          />
        </div>
      </Navbar>
    </>
  );
};

export default HeaderComponent;
