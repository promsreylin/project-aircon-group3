import React from 'react'
import FooterComponent from './footer/FooterComponent'
import HeaderComponent from './header/headerComponent'
import { Outlet } from 'react-router-dom'
import ScrollTopComponent from './scroll-top'


function LayoutComponent() {
  return (
    <>
    <HeaderComponent/>
    <Outlet/>
    <FooterComponent/>
    <ScrollTopComponent/>
    </>
  )
}

export default LayoutComponent