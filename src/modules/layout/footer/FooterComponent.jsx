import React from "react";
import icon from "../../../assets/img/icon-02-light.png";
import style from "../../../assets/css/yourStyleModule.module.css";
import { Link } from "react-router-dom";
function FooterComponent() {
  return (
    <div>
      <div
        className={`container-fluid ${style.background} footer mt-5 pt-5 blog data-aos="fade-right"`}
      >
        <div className="container py-5 ">
          <div className="row g-5">
            <div className="col-md-6">
              <h1 className="text-white mb-4 h1">
                <img src={icon} alt="Aircon" />
                Aircon
              </h1>
              <span className="text-secondary">
                Diam dolor diam ipsum sit. Aliqu diam amet diam et eos. Clita
                erat ipsum et lorem et sit, sed stet lorem sit clita. Diam dolor
                diam ipsum sit. Aliqu diam amet diam et eos. Clita erat ipsum et
                lorem et sit.
              </span>
            </div>
            <div className="col-md-6">
              <h5 className="text-light mb-4">Newsletter</h5>
              <p className="text-secondary">
                Clita erat ipsum et lorem et sit, sed stet lorem sit clita.
              </p>
              <div className="position-relative">
                <input
                  className={`${style.form} bg-transparent w-100 py-3 ps-4 pe-5 text-bg-secondary`}
                  type="text"
                  id="email"
                  name="email"
                  placeholder="Your Email"
                />
                <button
                  type="button"
                  className={`${style.btn}  py-2 px-3 position-absolute top-0 end-0 mt-2 me-2`}
                >
                  Sing up
                </button>
              </div>
            </div>
            <div className="col-lg-3 col-md-6  ">
              <h5 className="text-light mb-4 "> Get In Touch</h5>
              <div className="text-secondary">
                <p className="text-secondary">
                <i className="fa fa-map-marker-alt me-3"></i>123 Street, New
                  York, USA
                </p>
                <p className="text-secondary">
                  <i className="fa fa-phone-alt me-3"></i>+012 345 67890
                </p>
                <p className="text-secondary">
                  <i className="fa fa-envelope me-3"></i>info@example.com
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 text-decoration-none ">
              <h5 className="text-light mb-4">Our Services</h5>
              <Link className={`${style.btnlink}`} href="">
                AC Installation
              </Link>
              <Link className={`${style.btnlink}`} href="">
                Cooling Services
              </Link>
              <Link className={`${style.btnlink}`} href="">
                Heating Services
              </Link>
              <Link className={`${style.btnlink}`} href="">
                Annual Inspections
              </Link>
            </div>
            <div className="col-lg-3 col-md-6 ">
              <h5 className="text-light mb-4">Quick Links</h5>
              <Link className={`${style.btnlink}`} href="">
                About Us
              </Link>
              <Link className={`${style.btnlink}`} href="">
                Contact Us
              </Link>
              <Link className={`${style.btnlink}`} href="">
                Our Services
              </Link>
              <Link className={`${style.btnlink}`} href="">
                Trems & Condition
              </Link>
            </div>
            <div className="col-lg-3 col-md-6 ">
              <h5 className="text-light mb-4">Follow Us</h5>
              <div className="align-items-center gap-2 d-flex">
                <Link className={`${style.icon} `} href="">
                  <i className="fab fa-twitter "></i>
                </Link>
                <Link className={`${style.icon} `} href="">
                  <i className="fab fa-facebook "></i>
                </Link>
                <Link className={`${style.icon} `} href="">
                  <i className="fab fa-youtube "></i>
                </Link>
                <Link className={`${style.icon} `} href="">
                  <i className="fab fa-linkedin "></i>
                </Link>
              </div>
            </div>
            <div className={` contrainer-fulid ${style.copyright}`}>
              <div className="comtainer">
                <div className="row">
                  <div className="col-md-6 col-lg-6 text-md-start text-secondary text-center">
                    ©{" "}
                    <Link
                      className="text"
                      href="#"
                      style={{ textDecoration: "none", color: "#ff800f" }}
                    >
                      Your Site Name
                    </Link>
                    , All Right Reserved.
                  </div>

                  <div className="col-md-6 text-md-end text-secondary text-center pt-2">
                    Designed By
                    <Link
                      className="text"
                      href="#"
                      style={{ textDecoration: "none", color: "#ff800f" }}
                    >
                      HTML Codex
                    </Link>
                    Distributed by
                    <Link
                      href="#"
                      style={{ textDecoration: "none", color: "#ff800f" }}
                    >
                      ThemeWagon
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FooterComponent;
